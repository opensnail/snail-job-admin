const englishTranslations: I18n.Translations = {
  field: {
    second: 'Second',
    minute: 'Minute',
    hour: 'Hour',
    date: 'Date',
    month: 'Month',
    week: 'Week',
    year: 'Year'
  },
  fieldAlias: {
    second: 'second',
    minute: 'minute',
    hour: 'hour',
    date: 'date',
    month: 'month',
    week: 'week',
    year: 'year'
  },
  type: {
    empty: 'Empty',
    every: 'Every ',
    unspecific: 'Unspecific',
    range: ['From ', ' to ', ''],
    step: ['Start with ', ', execute every', ''],
    well: ['The ', ''],
    weekday: ['Nearest weekday to the ', ' of current month'],
    lastWeekday: 'Last weekday of current month',
    lastDayOfDate: 'Last day of current month',
    lastDayOfWeek: 'Last ',
    specify: 'Specify'
  },
  week: {
    Sunday: 'Sunday',
    Monday: 'Monday',
    Tuesday: 'Tuesday',
    Wednesday: 'Wednesday',
    Thursday: 'Thursday',
    Friday: 'Friday',
    Saturday: 'Saturday'
  },
  expression: 'The complete expression',
  preview: ['Last ', ' runtimes'],
  previewError: 'This expression is temporarily unparsed!'
};

export default englishTranslations;
