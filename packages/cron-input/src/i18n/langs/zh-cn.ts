const chineseTranslations: I18n.Translations = {
  field: {
    second: '秒',
    minute: '分',
    hour: '时',
    date: '日',
    month: '月',
    week: '周',
    year: '年'
  },
  fieldAlias: {
    second: '秒钟',
    minute: '分钟',
    hour: '小时',
    date: '天',
    month: '个月',
    week: '星期',
    year: '年'
  },
  type: {
    empty: '不指定',
    every: '每',
    unspecific: '不指定',
    range: ['从', '到', ''],
    step: ['从', '开始，每', '执行一次'],
    well: ['当月第', '个'],
    weekday: ['离当月', '号最近的那个工作日'],
    lastWeekday: '当月最后一个工作日',
    lastDayOfDate: '当月最后一天',
    lastDayOfWeek: '当月最后一个',
    specify: '指定'
  },
  week: {
    Sunday: '星期日',
    Monday: '星期一',
    Tuesday: '星期二',
    Wednesday: '星期三',
    Thursday: '星期四',
    Friday: '星期五',
    Saturday: '星期六'
  },
  expression: '完整表达式',
  preview: ['最近', '次运行时间'],
  previewError: '此表达式暂时无法解析！'
};

export default chineseTranslations;
